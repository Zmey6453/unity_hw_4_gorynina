using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RPG
{

[ExecuteInEditMode]
public class FollowingCamera : MonoBehaviour
{
  public Transform target;

  public Vector3 offset;

  void LateUpdate()
  {
    transform.position = target.position + offset;
  }
}

}
