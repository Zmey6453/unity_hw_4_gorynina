using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Game2
{

public class Projectile : MonoBehaviour
{
  [System.NonSerialized]
  public Unit target;
  public float speed;

  public Action on_die;

  public ParticleSystem die;

  void Update()
  {
    if(target == null)
      return;

    var dt = Time.deltaTime;
    var frame_dist = speed * dt;
    var dist = target.transform.position - transform.position;
    if(dist.magnitude < frame_dist)
    {
      transform.position = target.transform.position;
      target = null;
      StartCoroutine(Die());
    }
    else
      transform.position += dist.normalized * frame_dist;
  }

  IEnumerator Die()
  {
    yield return null;

    if(die != null)
    {
      var ps = ParticleSystem.Instantiate(die);
      ps.transform.position = transform.position;
    }

    on_die?.Invoke();
    Destroy(gameObject);
  }
}

}
